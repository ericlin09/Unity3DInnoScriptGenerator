﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

public class ScriptGenerator : MonoBehaviour
{
    [PostProcessBuildAttribute(999)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
#if UNITY_STANDALONE_WIN
        string pathOfScript = Application.dataPath + "/InnoScriptGenerator/innoScript.iss";

        string exe = pathToBuiltProject.Substring(pathToBuiltProject.LastIndexOf('/') + 1);
        string path = (pathToBuiltProject.Substring(0, pathToBuiltProject.LastIndexOf('/'))).Replace("/", @"\");
        string unityPlayer = "UnityPlayer.dll";
        string data = exe.Substring(0, exe.IndexOf(".")) + "_Data";

        string AppPublisher = Application.companyName;
        string AppName = Application.productName;
        string AppVersion = Application.version;

        string DefaultDirName = "{pf64}\\" + AppName;
        string DefaultGroupName = AppName;

        string UninstallDisplayIcon = "{app}\\" + exe;
        string Compression = "lzma2";

        string OutputBaseFilename = "installer";
        string SourceDir = path;
        string OutputDir = ".";

        try
        {
            if (File.Exists(pathOfScript))
            {
                File.Delete(pathOfScript);
            }

            using (FileStream fs = File.Create(pathOfScript))
            {
                string input = "";

                input += "\n[Setup]\n";
                input += GetMemberName(() => AppPublisher) + "=" + AppPublisher + "\n";
                input += GetMemberName(() => AppName) + "=" + AppName + "\n";
                input += GetMemberName(() => AppVersion) + "=" + AppVersion + "\n";
                input += GetMemberName(() => DefaultDirName) + "=" + DefaultDirName + "\n";
                input += GetMemberName(() => DefaultGroupName) + "=" + DefaultGroupName + "\n";
                input += GetMemberName(() => UninstallDisplayIcon) + "=" + UninstallDisplayIcon + "\n";
                input += GetMemberName(() => Compression) + "=" + Compression + "\n";
                input += GetMemberName(() => OutputBaseFilename) + "=" + OutputBaseFilename + "\n";
                input += GetMemberName(() => SourceDir) + "=" + SourceDir + "\n";
                input += GetMemberName(() => OutputDir) + "=" + OutputDir + "\n";

                input += "\n[Files]\n";
                input += SetFiles(path, exe, false, true);
                input += SetFiles(path, unityPlayer, false, true);
                input += SetFiles(path, data, true, true);

                input += "\n[Icons]\n";
                input += "Name: \"{group}\\" + AppName + "\"; Filename: \"{app}\\" + exe + "\"\n";
                input += "Name: \"{commondesktop}\\" + AppName + "\"; Filename: \"{app}\\" + exe + "\"\n";
                input += "Name: \"{commonprograms}\\" + AppName + "\"; Filename: \"{app}\\" + exe + "\"\n";
                input += "Name: \"{commonstartmenu}\\" + AppName + "\"; Filename: \"{app}\\" + exe + "\"\n";
                input += "Name: \"{commonstartup}\\" + AppName + "\"; Filename: \"{app}\\" + exe + "\"\n";

                input += "\n[RUN]\n";
                input += SetRun(AppName, exe);

                Byte[] info = new UTF8Encoding(true).GetBytes(input);
                fs.Write(info, 0, info.Length);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
#endif
    }

    static string SetFiles(string path, string name, bool isDirs, bool isIgnoreVersion)
    {
        string output = "";

        output = "Source: " + "\"" + path + "\\" + (isDirs ? (name + "\\*") : name) + "\"; DestDir: " + "\"{app}";

        if (isDirs)
            output += "\\" + name + "\"; ";
        else
            output += "\"; ";

        output += "Flags: ";

        if (isIgnoreVersion)
            output += "ignoreversion ";

        if (isDirs)
            output += "recursesubdirs createallsubdirs ";

        return output + "\n";
    }

    static string SetRun(string appName, string exe)
    {
        string output = "";

        output += "Filename: " + "\"{app}\\" + exe + "\"; ";

        output += "Description: " + "\"{cm:LaunchProgram," + appName + "}\"; ";

        output += "Flags: nowait postinstall skipifsilent";

        return output + "\n";
    }

    static string GetMemberName<T>(Expression<Func<T>> memberExpression)
    {
        MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
        return expressionBody.Member.Name;
    }
}
