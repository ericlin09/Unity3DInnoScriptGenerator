
[Setup]
AppPublisher=TopRoot,Inc.
AppName=Jackpot
AppVersion=1.0
DefaultDirName={pf64}\Jackpot
DefaultGroupName=Jackpot
UninstallDisplayIcon={app}\jackpot.exe
Compression=lzma2
OutputBaseFilename=installer
SourceDir=D:\Git\tr-unity-EW2018-demo\Win
OutputDir=.

[Files]
Source: "D:\Git\tr-unity-EW2018-demo\Win\jackpot.exe"; DestDir: "{app}"; Flags: ignoreversion 
Source: "D:\Git\tr-unity-EW2018-demo\Win\UnityPlayer.dll"; DestDir: "{app}"; Flags: ignoreversion 
Source: "D:\Git\tr-unity-EW2018-demo\Win\jackpot_Data\*"; DestDir: "{app}\jackpot_Data"; Flags: ignoreversion recursesubdirs createallsubdirs 

[Icons]
Name: "{group}\Jackpot"; Filename: "{app}\jackpot.exe"
Name: "{commondesktop}\Jackpot"; Filename: "{app}\jackpot.exe"
Name: "{commonprograms}\Jackpot"; Filename: "{app}\jackpot.exe"
Name: "{commonstartmenu}\Jackpot"; Filename: "{app}\jackpot.exe"
Name: "{commonstartup}\Jackpot"; Filename: "{app}\jackpot.exe"

[RUN]
Filename: "{app}\jackpot.exe"; Description: "{cm:LaunchProgram,Jackpot}"; Flags: nowait postinstall skipifsilent
